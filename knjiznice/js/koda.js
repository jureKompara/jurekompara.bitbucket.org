var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
/*global $*/
/*global btoa*/
/*global gauge1*/


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


function getName(ehrId) {
  var name;
	$.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  var searchData = [{key: "ehrId", value: ehrId}];
  $.ajax({
    async: false,
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (data) {
      var party = data.parties[0];
      //console.log(data);
      name = party["firstNames"];
    }
  });
  return name;
}

function getSurname(ehrId) {
  var surname;
	$.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  var searchData = [{key: "ehrId", value: ehrId}];
  $.ajax({
    async: false,
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (data) {
      var party = data.parties[0];
      surname = party["lastNames"];
    }
  });
  return surname;
}

function getFat(ehrId) {
  var weight;
  $.ajax({
    async: false,
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      weight = res[0].weight;
    }
  });
  return weight;
}

function getTall(ehrId) {
  var height;
  $.ajax({
    async: false,
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      height = res[0].height;
    }
  });
  return height;
}

function izpisi(ehrId){
  console.log(getName(ehrId) + " " + getSurname(ehrId) + " " + getFat(ehrId) + " " + getTall(ehrId));
}

function racunajBMI(){
  
  var ehrId = $("#EHRIDzaIzracunat").val();
  var vis = getTall(ehrId)/100.0;
  var fat = getFat(ehrId);
  var bmi = fat/(vis*vis);
  if(bmi > 30){
    var sporocilo = " (BMI je dosti previsok)";
  }
  else if(bmi > 25) {
    var sporocilo = " (BMI je previsok)";
  }
  else if(bmi > 18.5) {
    var sporocilo = " (BMI je normalen)";
  }
  else {
    var sporocilo = " (BMI je prenizek)";
  }
  
  console.log("visina: " + vis);
  
  console.log("fetina: " + fat);
  console.log("BMI: " + bmi);
  if(bmi > 0){
    gauge1.update(bmi);
    $("#izpis").html("");
    $("#izpis").append("Prikazan je BMI za: "+ getName(ehrId)+ " " + getSurname(ehrId) + sporocilo);
    
    
  }
  else{
    alert("Vpišite veljaven ehrID!");
  }
}



function napolniSelect() {
  var ehr = "";
  ehr = $("#izberiEhrID").val();
  ehr = ehr.substring(0, 36);
  $("#EHRIDzaIzracunat").val(ehr);
  //console.log("jaja");
}



var alertMsg = "Generirani so bili podatki za:\n";
var toThree = 0;

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function updatePatient(ehrId, visina, novaTeza, datum) {
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  var noviPodatki = {
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": datum,
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": novaTeza,
  };
  var nekiParametri = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT'
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(nekiParametri),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(noviPodatki),
    success: function(res) {
      //console.log(res.meta.href);
    },
    error: function(err) {
      console.log(err.responseText);
    }
  });
}


function doItTrikrat() {
  var select = document.getElementById("izberiEhrID");
  var length = select.options.length;
  for(var i = length; i >= 0; i--) {
    select.remove(i);
  }
  var opt = document.createElement('option');
  opt.innerHTML = "";
  select.appendChild(opt);
  alertMsg = "Generirani so bili podatki za:\n";
  toThree = 0;
  generirajPodatke(1);
  generirajPodatke(2);
  generirajPodatke(3);
}

function generirajPodatke(stPacienta) {
  var ehrId = "";
  var ime;
  var priimek;
  var datumRojstva;
  if (stPacienta == 1) {
    ime = "Varg";
    priimek = "Vikernes";
    datumRojstva = "1982-07-18";
  }
  else if (stPacienta == 2) {
    ime = "Zark";
    priimek = "Muckerberg";
    datumRojstva = "1982-07-18";
  }
  else {
    ime = "Jezen";
    priimek = "Janez";
    datumRojstva = "1999-02-11";
  }

  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(data) {
      ehrId = data.ehrId;
      alertMsg += ime + " " + priimek + "\n";
      toThree++; 
      console.log(ime + " " + priimek + ": "+ ehrId);
      if (toThree == 3) {
        alert(alertMsg);
      }

        var select = document.getElementById('izberiEhrID');
        var opt = document.createElement('option');
        opt.innerHTML = ehrId + " (" + ime + " " + priimek + ")";
        select.appendChild(opt);
      //$("#header").html("EHR: " + ehrId);

      // build party data
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: { "ehrId": ehrId }
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
            if (stPacienta == 1) {

              updatePatient(ehrId, "181.4", "80.2", "2014-05-20");
              updatePatient(ehrId, "182.4", "83.7", "2015-05-20");
              updatePatient(ehrId, "183.3", "82.5", "2016-05-20");
            }

            else if (stPacienta == 2) {
              updatePatient(ehrId, "165.5", "99.7", "2015-05-20");
              updatePatient(ehrId, "165.4", "100.1", "2016-05-20");
            }

            else if (stPacienta == 3) {
              updatePatient(ehrId, "165.3", "48.0", "2016-02-20");
              updatePatient(ehrId, "167.6", "47.4", "2017-02-20");
              updatePatient(ehrId, "168.9", "42.3", "2017-03-20");
            }
            //$("#result").html("Created: " + party.meta.href);
          }
        }

      });
    }
  });
}
